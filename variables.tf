variable "a-records" {
  type = map
  default = {
    "www"     = "1.2.3.4",
    "demo"    = "22.44.66.88",
    "staging" = "12.34.56.78"
  }
}
