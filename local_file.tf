resource "local_file" "foobar" {
  content         = "Hello World!"
  filename        = "/tmp/foobar.txt"
  file_permission = "0550"

  provisioner "local-exec" {
    command = "echo $MSG $(date) > /tmp/foobar-run.txt"
    environment = {
      MSG = "time: "
    }
    interpreter = ["bash", "-c"]
  }
}

output "path" {
  value = local_file.foobar.filename
}

resource "local_file" "a-records" {
  for_each = var.a-records

  content         = "${each.key}  A   ${each.value}"
  filename        = "/tmp/record-${each.key}.txt"
  file_permission = "0550"
}

output "sample" {
  value = local_file.a-records["www"].content
}
