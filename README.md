# Terraform demo project

This project contains some basics for getting started with Terraform.

## Git hooks

### pre-commit

Due to Terraform prescribing a desired format of Terraform files it is useful to stick to that convention. The `terraform fmt` command makes that easy, but it does add another step to submitting changes. Enter git hooks, which are scripts that run on a specific git event, like _pre-commit_. This code describes a git hook which can:

* check your code and output differences with the desired format, or;
* format your staged code, or;
* both

Copy this code in a file called `.git/hooks/pre-commit` and set the executable bit:

```
# capture staged files so we can re-add them later
staged_files=$(git diff --name-only --staged)

# iterate over directories containing staged files
for dir in $(echo "$staged_files" | xargs dirname | uniq)
do
  # check staged files for formatting errors and return diff
  #terraform fmt -check -diff "$dir"

  # reformat directories containing staged files
  terraform fmt "$dir"
done

# re-add files in case they have been reformatted
echo "$staged_files" | xargs git add
```
